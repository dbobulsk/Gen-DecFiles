# Deadline for next release

Deadline for contributions to DecFiles v30r10 is Friday 26th January 2018 at 14:00. Merge requests created after deadline are not guaranteed to be accepted for this release.

# Steps to add decay file
1. Find if a DecFile you want (or similar enough so you can use it) already exists
1. Read the [event type numbering convention](https://cds.cern.ch/record/855452?ln=en) or check out [the (unofficial) event type finder](http://eventtype.boosted.science/).
1. If you are using an old DecFile as a template, note that it might not follow the established conventions.
1. Test your DecFile (both that it runs and that it produces output you want)
1. After you've modified the release notes in the doc folder of the package commit to branch and create merge request against master.
1. Day or two after commit check in [nightlies](https://lhcb-nightlies.cern.ch/nightly/summary/) slot lhcb-decfilestests that
  1. there are no warning or errors from your decay file at build stage
  1. your file passes test. Look for positive evidence as that will also allow you to check that file was really committed.
  1. If anything fails, please correct and recheck again day after commit. If you do not understand failure, get in touch with lhcb-gauss-manager@cernNOSPAMPLEASE.ch
1. Email the lhcb-gauss-manager mailing list with a link to your DecFile after you placed it in a publicly accessible location on the AFS (eg. ~/public). They will also check and have the last word on the event type ID (some some special cases or newer rules may not be covered in 2). (We are going to update this step eventually, but for now it is helpful if we do not have to checkout your branch locally).

# Testing a decay file


# Commiting a decay file


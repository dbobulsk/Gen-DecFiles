# EventType: 15266041 
# 
# Descriptor: [Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) pi- pi+ pi-]cc
# 
# NickName: Lb_Lc3pi,pKpi=DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation: Lb -> Lc 3pi, Lc to p K pi. 
# The Lambda_c is forced to the p+ K- pi+ final state, through several intermediate resonances.
# All charged final state tracks are required to be within the LHCb acceptance.
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# CPUTime: 4 min
# Responsible: Daniel Vieira
# Email: devieira@ucas.ac.cn
# Date: 20170127
#

# Define Lambda_c
Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-

# Define Lambda(1520)0
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0

# Define K*0
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0

# Define Delta++
Alias      MyDelta++      Delta++
Alias      Myanti-Delta-- anti-Delta--
ChargeConj MyDelta++      Myanti-Delta--

# Define Lambda_b0 decay
Decay Lambda_b0sig
1.000        MyLambda_c+ pi- pi+ pi-    PHSP;
Enddecay
CDecay anti-Lambda_b0sig

# Define Lambda_c+ decay
# Branching ratios from PDG 2016
Decay MyLambda_c+
0.03500 p+              K-         pi+ PHSP;
0.01980 p+              Myanti-K*0     PHSP;
0.01090 MyDelta++       K-             PHSP;
0.02200 MyLambda(1520)0 pi+            PHSP;
Enddecay
CDecay Myanti-Lambda_c-
    
# Define Lambda(1520)0 decay
Decay MyLambda(1520)0
1.000 p+ K- PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
    
# Define K*0 decay
Decay MyK*0
1.000 K+ pi- VSS;
Enddecay
CDecay Myanti-K*0
    
# Define Delta++ decay
Decay MyDelta++
1.000 p+ pi+ PHSP;
Enddecay
CDecay Myanti-Delta--
End

#
# EventType: 11465023
#
# Descriptor: [B0 -> (D0b -> pi- pi+ pi- pi+) X]cc
#
# NickName: Bd_D0X,X=cocktail,D0=PHSP,DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation: Low mass background B0 -> D0b[pi- pi+ pi- pi+] X decays of the B+ -> D0b[pi- pi+ pi- pi+] h+ signal channel. The D0b decays via phasespace. This file is intended to be expanded upon as the data sample increases to allow more low mass backgrounds to be considered.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: <1min
# Responsible: Claire
# Email: claire.prouve@cern.ch
# Date: 20170314
#

Alias		My_D0		D0
Alias		My_D*+		D*+
Alias		My_anti-D0	anti-D0
Alias		My_anti-D*+	D*-
ChargeConj	My_D0		My_anti-D0
ChargeConj	My_D*+		My_anti-D*+

# Scale to account for modified D* branching fraction
# B0 -> D*- pi+: 0.00276*0.677
# B0 -> D*- rho+: 0.0068*0.677
# B0 -> D*- K+: 0.000214*0.677

Decay B0sig
0.00032		rho0		My_anti-D0	SVS;
0.00187		My_anti-D*+	pi+		PHOTOS SVS;
0.00460		My_anti-D*+	rho+		SVV_HELAMP 0.152 1.47 0.936 0.0 0.317 0.19;
0.000145	My_anti-D*+ 	K+		PHOTOS SVS;
Enddecay
CDecay anti-B0sig

Decay My_D*+
1.0   My_D0 pi+	PHOTOS VSS;
Enddecay
CDecay My_anti-D*+

Decay My_D0
1.0   pi+ pi- pi+ pi-	PHSP;
Enddecay
CDecay My_anti-D0

End

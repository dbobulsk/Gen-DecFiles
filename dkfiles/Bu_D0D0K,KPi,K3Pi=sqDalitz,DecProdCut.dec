# EventType: 12197069
# NickName: Bu_D0D0K,KPi,K3Pi=sqDalitz,DecProdCut 
# Descriptor: [B+ -> ( D0 -> K- pi+ ) ( D~0 -> K+ pi- pi- pi+ ) K+]cc
#
# Cuts: DaughtersInLHCb
#
# Documentation:
#    Decay file for B+- -> D0 D0bar K+- , D0 of 'opposite-sign' as B goes to K3pi. B decay forced flat in sq Dalitz plot.
# EndDocumentation
#
# Date:   20170619
#
# Responsible: Pavol Stefko
# Email: pavol.stefko@cern.ch
# PhysicsWG: B2OC
# CPUTime: < 1 min
#
# Tested: Yes

Alias My_D0   			D0
Alias My_anti-D0 		anti-D0
Alias My_D0_Kpi   		D0
Alias My_anti-D0_Kpi 	anti-D0
Alias My_D0_K3pi   		D0
Alias My_anti-D0_K3pi 	anti-D0

ChargeConj My_anti-D0 		My_D0
ChargeConj My_anti-D0_Kpi 	My_D0_Kpi
ChargeConj My_anti-D0_K3pi 	My_D0_K3pi

Decay My_D0_Kpi
  1.0 K- pi+ 		 		PHSP;
Enddecay
CDecay My_anti-D0_Kpi

Decay My_D0_K3pi
  1.0 K- pi+ pi+ pi-		PHSP;
Enddecay
CDecay My_anti-D0_K3pi

Decay B+sig
  1.0 My_D0_Kpi My_anti-D0_K3pi K+ 	FLATSQDALITZ;
Enddecay
CDecay B-sig

End
